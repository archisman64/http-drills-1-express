const http = require('http');
const fs = require('fs');
const path = require('path');
const express = require('express');

const expressRequestId = require('express-request-id');

const crypto = require('crypto');

const app = express();
app.use(expressRequestId());
app.use(logRequests);

const { port } = require('./config');

const pathToLogFile = path.join(__dirname, 'request-logs.log');

function logRequests (req, res, next) {
  const logEntry = `${req.id} - ${new Date().toISOString()} - ${req.method} ${req.originalUrl}\n`;
  fs.appendFile(pathToLogFile, `${logEntry}\n`, (err) => {
    if(err) {
      console.log(err);
      next(err);
    } else {
      next();
    }
  })
}

// Custom error handling middleware
function myErrorHandler (err, req, res, next) {
  console.log('my error handler ran');
  const statusCode = err.statusCode || 500;
  res.status(statusCode).send(err.message || { error: { message: 'Internal Server Error' } });
}

app.get('/', (req, res, next) => {
  const htmlContent = `
    <!DOCTYPE html>
    <html>
      <head>
        <title>Home</title>
      </head>
      <body>
          <h1>Welcome to Archisman's website!</h1>
          <h2>You can try the following endpoints here:</h2>
          <ul>
            <li><a href="/html">/html</a></li>
            <li><a href="/json">/json</a></li>
            <li><a href="/uuid">/uuid</a></li>
            <li><a href="/status/200">/status/default</a></li>
            <li><a href="/delay/1">/delay/default</a></li>
            <li><a href="/logs">/logs</a></li>
          </ul>
      </body>
    </html>
  `;
  res.send(htmlContent);
});

app.get('/html', (req, res, next) => {
  const htmlContent = `
    <!DOCTYPE html>
    <html>
      <head>
      </head>
      <body>
          <h1>Any fool can write code that a computer can understand. Good programmers write code that humans can understand.</h1>
          <p> - Martin Fowler</p>
      </body>
    </html>
  `;
  res.send(htmlContent);  
});

app.get('/json', (req, res, next) => {
  const jsonData = {
    slideshow: {
      author: 'Yours Truly',
      date: 'date of publication',
      slides: [
        {
          title: 'Wake up to WonderWidgets!',
          type: 'all',
        },
        {
          items: [
            'Why <em>WonderWidgets</em> are great',
            'Who <em>buys</em> WonderWidgets',
          ],
          title: 'Overview',
          type: 'all',
        },
      ],
      title: 'Sample Slide Show',
    },
  };
  res.json(jsonData);
});

app.get('/uuid', (req, res, next) => {
  const generatedUuid = crypto.randomUUID();
  const responseData = { uuid: generatedUuid };
  res.json(responseData);
});

app.get('/status/:statusCode', (req, res, next) => {
  const statusCode = parseInt(req.params.statusCode);
  if (http.STATUS_CODES[statusCode]) {
    const htmlContent = `
      <!DOCTYPE html>
      <html>
        <head>
        </head>
        <body>
            <h2>${statusCode}: ${http.STATUS_CODES[statusCode]}</h2>
        </body>
      </html>
    `;
    res.status(statusCode).send(htmlContent);
  } else {
    const htmlContent = `
      <!DOCTYPE html>
      <html>
        <head>
        </head>
        <body>
            <h2>Bad Request</h2>
            <h4>The code you requested is not a valid HTTP status code.</h4>
        </body>
      </html>
    `;
    next({ statusCode: 400, message: htmlContent });
  }
});

app.get('/delay/:delayInSeconds', (req, res, next) => {
  const delayInSeconds = Number(req.params.delayInSeconds);
  if (
    (delayInSeconds !== 0 && !delayInSeconds) ||
    delayInSeconds < 0 ||
    delayInSeconds > 10
  ) {
    const htmlContent = `
      <!DOCTYPE html>
      <html>
        <head>
        </head>
        <body>
            <h3>Please enter a valid delay</h3>
            <h2>Valid Range: 0 - 10 seconds</h2>
        </body>
      </html>
    `;
    
    next({ statusCode: 400, message: htmlContent });
  } else {
    setTimeout(() => {
      const htmlContent = `
        <!DOCTYPE html>
        <html>
          <head>
          </head>
          <body>
              <h3>THIS IS A DELAYED RESPONSE BY ${delayInSeconds} SECS!</h3>
          </body>
        </html>
      `;
      res.send(htmlContent);
    }, delayInSeconds * 1000);
  }
});

app.get('/logs', (req, res, next) => {
  // sending the contents of log file as response
  res.sendFile(pathToLogFile, (err) => {
    if(err) {
      next(err);
    }
  });
});

app.use((req, res, next) => {
  const htmlContent = `
    <!DOCTYPE html>
    <html>
      <head>
      </head>
      <body>
          <h2>Requested endpoint is not supported!</h2>
          <h3>Please enter a valid endpoint or <a href="/">click here to go to home</a></h3>
      </body>
    </html>
  `;
  next({ statusCode: 404, message: htmlContent });
});

app.use(myErrorHandler);

app.listen(port, () => {
  console.log('Server is running!');
});